/**
 * Created by liqian on 2014-09-13.
 */
/**
 * Main类
 * @author lq
 **/
if(LGlobal.canTouch){
    LGlobal.stageScale = LStageScaleMode.EXACT_FIT;
   
    LSystem.screen(LStage.FULL_SCREEN);
}

//滚动条
function doScroll() {
    if(window.pageYOffset === 0) {
        window.scrollTo(0, 1);
    }
}
window.onload = function() {
    setTimeout(doScroll, 100);
    init(50,"legend",480,800,main,LEvent.INIT);
}
window.onorientationchange = function() {
    setTimeout(doScroll, 100);
};
window.onresize = function() {
    setTimeout(doScroll, 100);
}

var gwBackLayer;
var loadingLayer;//loadingLayer 层
var imglist = {};
var backgroud;//游戏背景


var menuIndex=0;//章节
var stageIndex=0;//当前积分


var imgData = new Array(
    {name:"sound",path:"./js/SoundPlayer.js"}
);


function main() {
    gwBackLayer = new LSprite();
    gwBackLayer.color="#FFFFFF";
//    var gwBgBitmap = new LBitmap(new LBitmapData(imglist["gwBg1"]));
//    gwBackLayer.addChild(gwBgBitmap);
    addChild(gwBackLayer);

    loadingLayer = new LoadingSample3();
    gwBackLayer.addChild(loadingLayer);

    LLoadManage.load(
        imgData,
        function (progress) {
            loadingLayer.setProgress(progress);
        },
       gameInit
    );
}
var soundBg;
var soundFlag = true;
function gameInit(result){
    imglist = result;
    gwBackLayer.removeChild(loadingLayer);
    loadingLayer = null;

    backgroud = new LSprite();
    addChild(backgroud);

    soundBg = new SoundPlayer();
    soundBg.loadSound();

    gameLogoShow();
}
function gameLogoShow(){
    var logoLayer = new GameMenu();
    backgroud.addChild(logoLayer);
}
///**
// * 菜单
// */
//function menuShow(){
//    backgroud.removeAllChild();
//    backgroud.die();
//
//    var menuLayer = new GameMenu();
//    backgroud.addChild(menuLayer);
//    soundBg.loadSound();
//    soundBg.playSound("click");
//}
///**
// * 关卡
// */
//function stageShow(){
//    backgroud.removeAllChild();
//    backgroud.die();
//
//    var stageLayer = new StageMenu();
//    backgroud.addChild(stageLayer);
//    soundBg.loadSound();
//    soundBg.playSound("click");
//}
/**
*
* @param Index
*/
function gameShow(mIndex){
    menuIndex = mIndex;
    backgroud.removeAllChild();
    backgroud.die();

    var gameLayer = new Game(menuIndex);
    backgroud.addChild(gameLayer);
}












