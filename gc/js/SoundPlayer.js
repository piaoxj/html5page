/**
 * Created by MLS on 14-9-24.
 */
function SoundPlayer(){
    var self = this;
    self.loadIndex = 0;
    self.click = new LSound();
    self.click.parent = self;
    self.background = new LSound();
    self.background.parent = self;
}
SoundPlayer.prototype.loadSound = function(){
    var self = this;
    if(LGlobal.canTouch && self.loadIndex > 0 && self.loadIndex < 2)self.loadIndex = 1;
    if(self.loadIndex == 0){
        self.loadIndex++;
        self.backgroundLoad();
    }else if(self.loadIndex == 1){
        self.loadIndex++;
        self.click.addEventListener(LEvent.COMPLETE,self.clickLoadComplete);
        //self.click.load("./sound/fly.mp3");
    }
};
SoundPlayer.prototype.playSound = function(name){
    var self = this;
    switch(name){
        case "click":
            if(!self.clickIsLoad)return;
            if(soundFlag){
                //self.click.close();
                //self.click.play(0,1);
            }
            break;
        case "background":
            if(!self.backgroundIsLoad)return;
            self.background.close();
            //self.background.play(0,100);
            break;
    }
};
SoundPlayer.prototype.backgroundLoad = function(){
    var self = this;
    self.background.addEventListener(LEvent.COMPLETE,self.backgroundLoadComplete);
//    self.background.load("./sfx/music.ogg");
    //self.background.load("./sound/background.mp3");
};
SoundPlayer.prototype.backgroundLoadComplete = function(event){
    var self = event.currentTarget;
    self.parent.backgroundIsLoad = true;
    //self.play(0,100);
};
SoundPlayer.prototype.clickLoadComplete = function(event){
    var self = event.currentTarget;
    self.parent.clickIsLoad = true;
};
