/**
 * Created by MLS on 14-10-4.
 */

var times=0;//计时
var colorList=[];//用于存放每关的颜色，关卡结束销毁
var questionList=[];//用于存放问题的颜色和字，关卡结束销毁
var gameLayer;//游戏层
var answardNum=0;
var tempList=[];
function Game(menuIndex){
    base(this,LSprite,[]);
    var self = this;
    gameLayer = new LSprite();
    initCodes(menuIndex);
    makeQuestion();

    //时间
    times = new LTextField();
    times.font = "HG行書体";
    times.size = 30;
    times.x = 100;
    times.y = 30;
    times.text = "0";
    gameLayer.mindex = menuIndex;
    gameLayer.addChild(times);

    var si = new  LTextField();
    si.font = "HG行書体";
    si.size = 30;
    si.x = 250;
    si.y = 30;
    si.text = stageIndex;
    gameLayer.addChild(si);
    gameLayer.addEventListener(LEvent.ENTER_FRAME,onframe);
    self.addChild(gameLayer);
}
var startTime;
function onframe(event){
    var self = gameLayer;
    var timeLimit = 5;
    if(self.mindex==0){
        timeLimit = 5;
    }else if (self.mindex==1){
        timeLimit = 4;
    }else if (self.mindex==2){
        timeLimit = 3;
    }else{
        timeLimit = 2;
    }
    var currentTime = (new Date()).getTime();
    var time = currentTime-startTime;
    var minute = (time/(60*1000))>>>0;
    var second = ((time-minute*60*1000)/1000)>>>0;
    var millisecond = (time-minute*60*1000-second*1000)>>>0;
    var t = timeLimit - (second+"."+millisecond);
    if(t<=0){
        timeOut();
        t = 0;
    }
    times.text = t.toFixed(3);
}


function initCodes(menuIndex){

    getColor(menuIndex);
    startTime = new Date().getTime();
}


function getColor(menuIndex){
    switch (menuIndex){
        case 0:
                addColor(100,100,menuIndex,120);
            break;
        case 1:
                addColor(100,100,0,80);
                addColor(200,100,0,80);
            break;
        case 2:
                addColor(100,100,0,80);
                addColor(200,100,0,80);
                addColor(100,200,0,80);
                addColor(200,200,0,80);
            break;
        case 3:
                addColor(100,100,0,80);
                addColor(200,100,0,80);
                addColor(100,200,0,80);
                addColor(200,200,0,80);
                addColor(100,300,0,80);
                addColor(200,300,0,80);
                addColor(100,400,0,80);
                addColor(200,400,0,80);
            break;
    }
}

function addColor(x,y,menuIndex,s){
    var self = gameLayer;
    var l = codeList[menuIndex].length;

    var index = Math.floor(l*Math.random());
    var n = codeList[menuIndex][index].n;//随机的字用于展示
    index = Math.floor(l*Math.random());
    var c = codeList[menuIndex][index].c;
    var tn = codeList[menuIndex][index].n;//真实的字用于比较
    //var cn = codeList[menuIndex][index].cn;
    console.log("n:"+n+"c:"+c);

    var theTextField = new LTextField();
    theTextField.text=n;
    theTextField.size=s;
    theTextField.color=c;

    theTextField.x = x;
    theTextField.y = y;
    self.addChild(theTextField);
    colorList.push(c);
}



function makeQuestion(){
    var self = gameLayer;
    var colorArr = pepare(colorList);
//    var rand = Math.floor(colorArr.length*Math.random());
    if(colorArr.length==1){
        console.log("1这是什么颜色");
        var question = new LTextField();
        question.font = "HG行書体";
        question.size = 30;
        question.x = 50;
        question.y = 400;
        question.text = "这是什么颜色??";
        self.addChild(question);
        // 随机算法简单问题
//        var c;
        var index;
        for(var i=0;i<codeList[menuIndex].length;i++){
            if(codeList[menuIndex][i].c==colorArr[0]){
//                c = codeList[menuIndex][i].c;
                questionList.push(codeList[menuIndex][i]);
                index=codeList[menuIndex][i].n;
                break;
            }
        }
        var indexj=0;
        tempList = cList[0].slice(0);
        tempList.sort(function(){
            return Math.random()>.5?-1:1;
        });
        for(var j=0;j<codeList[menuIndex].length;j++){
            if(indexj==2) {
                break;
            }
            var t = tempList.pop();
            if(t.n!=index){
                questionList.push(t);
                indexj++;
            }
        }
        tempList=[];
        questionList.sort(function(){
            return Math.random()>.5?-1:1;
        });

        tempList = cList[0].slice(0);
        tempList.sort(function(){
            return Math.random()>.5?-1:1;
        });
        for(var m=0;m<questionList.length;m++){
            var t = tempList.pop();
            makeButton(t.n,questionList[m].c,m)
        }
        tempList=[];


    }else if(colorArr.length>1&&colorArr.length<3){
        console.log("2这里有什么颜色");
        var question = new LTextField();
        question.font = "HG行書体";
        question.size = 30;
        question.x = 50;
        question.y = 400;
        question.text = "这里有什么颜色??";
        self.addChild(question);
        // 随机算法中等问题


    }else if(colorArr.length>=3){
        console.log("3这里没有什么颜色");
        var question = new LTextField();
        question.font = "HG行書体";
        question.size = 30;
        question.x = 50;
        question.y = 550;
        question.text = "这里没有什么颜色??";
        self.addChild(question);
        // 随机算法困难问题


    }
}
function makeButton(n,c,i){
    var button = new LButtonSample1(n,30,"Arial",c);
    button.x = 90*i+50;
    button.y = 500;
    self.addChild(button);
    button.addEventListener(LMouseEvent.MOUSE_DOWN,answord);
}
/**
 * 清理本次游戏数据
 */
function clearThisGame(){
    times=0;//计时
    answardNum=0;//回答问题为0
    colorList=[];//用于存放每关的颜色，关卡结束销毁
    questionList=[];//用于存放问题的颜色和字，关卡结束销毁
    gameLayer.removeAllChild();
}
/**
 * 下个游戏
 */
function nextGame(){
    clearThisGame();
    gameShow(menuIndex);
}
/**
 * 超时
 */
function timeOut(){
    stageIndex--;
    nextGame();
}
/**
 * 去重算法
 * @param receiveArray
 * @returns {Array}
 */
function pepare(receiveArray){
    var arrResult = new Array(); //定义一个返回结果数组.
    for (var i=0; i<receiveArray.length; ++i) {
        if(check(arrResult,receiveArray[i]) == -1) {
            //在这里做i元素与所有判断相同与否
            arrResult.push(receiveArray[i]);
            //　添加该元素到新数组。如果if内判断为false（即已添加过），
            //则不添加。
        }
    }
    return arrResult;
}
function check(receiveArray,checkItem){
    var index = -1; //　函数返回值用于布尔判断
    for(var i=0; i<receiveArray.length; ++i){
        if(receiveArray[i]==checkItem){
            index = i;
            break;
        }
    }
    return index;
}
/**
 * 第1种回答记分方式
 * 这是什么颜色??
 * @param event
 */
function answord(event){
    var colorArr = pepare(colorList);
    var self = event.currentTarget;
    if(self.labelText.color==colorArr[0]){
        stageIndex++;//正确
    }else{
        stageIndex--;
    }
    nextGame();
}


/**
 * 第2种回答记分方式
 * 这里有什么颜色??
 * @param event
 */
function answord2(event){
    var colorArr = pepare(colorList);
    var self = event.currentTarget;
    if(self.labelText.text==colorArr[0]){
        stageIndex++;//正确
    }else{
        stageIndex--;
    }
    nextGame();
}


/**
 * 第2种回答记分方式
 * 这里没有什么颜色??
 * @param event
 */
function answord3(event){
    var colorArr = pepare(colorList);
    var self = event.currentTarget;
    if(self.labelText.text==colorArr[0]){
        stageIndex++;//正确
    }else{
        stageIndex--;
    }
    nextGame();
}

