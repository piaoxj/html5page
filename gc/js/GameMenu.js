/**
 * Created by liqian on 2014-09-13.
 */


function GameMenu(){
    base(this ,LSprite,[]);
    var self = this;
    // 简单模式 button
    var button = new LButtonSample1("简单模式");
    button.x = 150;
    button.y = 100;
    self.addChild(button);
    button.addEventListener(LMouseEvent.MOUSE_DOWN,simpleGame);
    // 困难模式 button
    var button = new LButtonSample1("困难模式");
    button.x = 150;
    button.y = 150;
    self.addChild(button);
    button.addEventListener(LMouseEvent.MOUSE_DOWN,simpleGame);
    // 地狱模式 button
    var button = new LButtonSample1("地狱模式");
    button.x = 150;
    button.y = 200;
    self.addChild(button);
    button.addEventListener(LMouseEvent.MOUSE_DOWN,simpleGame);
    // 噩梦模式 button
    var button = new LButtonSample1("噩梦模式");
    button.x = 150;
    button.y = 250;
    self.addChild(button);
    button.addEventListener(LMouseEvent.MOUSE_DOWN,simpleGame);
};


function simpleGame(event){
    if(event.currentTarget.y==100){
        console.log("简单模式");
        menuIndex=0;
    }else if(event.currentTarget.y==150){
        console.log("困难模式");
        menuIndex=1;
    }else if(event.currentTarget.y==200){
        console.log("地狱模式");
        menuIndex=2
    }else{
        console.log("噩梦模式");
        menuIndex=3;
    }

    gameShow(menuIndex);
}

