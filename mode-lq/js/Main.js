/**
 * Created by laolao on 2015/1/5.
 */
/**
 * 游戏初始化设定
 **/

LInit(10, "mylegend", 640, 1136, main);
/**
 * 层变量
 * */
//显示进度条所用层
var loadingLayer;
//背景层
var backLayer;

var sound;

var dataList = {};
var loadData = new Array(
    /*首页*/
    {name: "ice_bgHome", path: "./images/bgHome.jpg"}
);
var ballList = [];
var mapXyist = [];
var checkBall = [];
var shakeBallList = [];
var linkBallList = [];
var oneBall = false;//屏幕中只有一个小球可以用
var ballObj;
function main() {
    if (LGlobal.canTouch) {
        LGlobal.stageScale = LStageScaleMode.NO_BORDER;
        LSystem.screen(LStage.FULL_SCREEN);

    }
    loadingLayer = new LoadingSample3();
    addChild(loadingLayer);

    $.getJSON("./res/resource.json", function (data) {
        LLoadManage.load(
            data,
            function (progress) {
                loadingLayer.setProgress(progress);
            },
            gameInit
        );
    })
};

function gameInit(result) {
    dataList = result;
    removeChild(loadingLayer);
    loadingLayer = null;
    initGame();
};

var homeLayer;
/**
 * 初始化游戏
 */
function initGame() {
    //背景层
    homeLayer = new LSprite();
    addChild(homeLayer);
    backLayer = new LSprite();

    addChild(backLayer);
    console.log(dataList["ice_bgHome"].src);
    dataList["ice_bgHome"].src="http://m.mengdogame.com/dabai/statics/img/page1/1.png";
    var a  =dataList["ice_bgHome"];

    var home = new LBitmap(new LBitmapData(dataList["ice_bgHome"]));
    homeLayer.addChild(home);

    var btnStart = new LSprite();
    homeLayer.addChild(btnStart);
    var btnStartBitmap = new LBitmap(new LBitmapData(dataList["btnStart"]));
    btnStart.addChild(btnStartBitmap);
    btnStart.x = 100;
    btnStart.y = 500;
    btnStart.addEventListener(LMouseEvent.MOUSE_DOWN,startBtn);

    var btnRank = new LSprite();
    homeLayer.addChild(btnRank);
    var btnRankBitmap = new LBitmap(new LBitmapData(dataList["btnRank"]));
    btnRank.addChild(btnRankBitmap);
    btnRank.x = 500;
    btnRank.y = 500;
    btnRank.addEventListener(LMouseEvent.MOUSE_DOWN,rankBtn);
};

function startBtn(e){
    homeLayer.removeAllChild();
    homeLayer.die();
    homeLayer=null;

    goMap();
    startGame1();
};

function goMap(){
    var bgMap = new LBitmap(new LBitmapData(dataList["bgMap"]));
};

function rankBtn(e){
    homeLayer.removeAllChild();
    homeLayer.die();
    //homeLayer=null;
    console.log(11);
    rankList("http://m.mengdogame.com/dabai/statics/img/page1/1.png","nihao",10);

};

function rankList(avatar,name,rank){
    dataList["avatar"].src=avatar;

    var rankLayer = new LSprite();
    homeLayer.addChild(rankLayer);

    var avatarBitmap =new LBitmap(new LBitmapData(dataList["avatar"]));
    rankLayer.addChild(avatarBitmap);

    var nameText = new LTextField();
    nameText.text=name;
    nameText.x = 100;
    nameText.y = 100;
    rankLayer.addChild(nameText);

    var rankText = new LTextField();
    rankLayer.addChild(rankText);
    rankText.x = 100;
    rankText.y = 200;
    rankText.text = rank;

}

function startGame1(){

    homeLayer.removeAllChild();
    homeLayer.die();
    homeLayer=null;
    var ice_bgHome = new LBitmap(new LBitmapData(dataList["ice_bgHome"]));
    backLayer.addChild(ice_bgHome);
    backLayer.addEventListener(LMouseEvent.MOUSE_DOWN, moveDown);
    backLayer.addEventListener(LEvent.ENTER_FRAME, onframe);

    initMap();
    showCenterColor();
}

function initMap() {
    var index = 0;
    for (var i = 0; i < mapArr.length; i++) {

        for (var j = 0; j < mapArr[i].length; j++) {
            if(mapArr[i][j]<=6){
                ballList[index] = new Ball(mapArr[i][j], i, j,0, 0, 0, 0);
                backLayer.addChild(ballList[index]);
                index++;
            }
        }
    }

    for (var i = 0; i < mapXY.length; i++) {

        for (var j = 0; j < mapXY[i].length; j++) {
            var obj = new MapObj(i, j);
            mapXyist.push(obj);
        }
    }
}
/**
 * 预警颜色
 */
var showColor;
var showColorBall;
function showCenterColor() {
    showColor = Math.round(Math.random() * 5);
    //console.log(showColor);
    showColorBall = new Ball(showColor, 1, 1,0, 0, 350, 800);
    backLayer.addChild(showColorBall);
}
/**
 * 用户操作
 * @param event
 */
function moveDown(event) {
    var directionX = event.offsetX - LGlobal.width*0.5;
    var directionY = LGlobal.height-event.offsetY;
    var degree  = Math.atan2(directionY, directionX);
    var angle = degree*180/Math.acos(-1.0);
    var vx =20*Math.cos(angle * Math.PI / 180);
    var vy =20*Math.sin(angle * Math.PI / 180);
    if (!oneBall) {
        var x = event.selfX;
        ballObj = new Ball(showColor, 1, 1,vx, vy, x, 800);
        backLayer.addChild(ballObj);
        oneBall = true;
        showCenterColor();
        //showColorBall.remove();
    }
};

/**
 * 比较距离
 * @param ballObj
 * @param ballTemp
 * @returns {number}
 */
function computBallXY(ballObj, ballTemp) {
    var y2c = ballTemp.y - ballObj.y;
    var x2c = ballTemp.x - ballObj.x;
    var result = Math.sqrt(Math.pow(Math.abs(y2c), 2) + Math.pow(Math.abs(x2c), 2));
    return result;
};


/**
 * 检查小球,消除算法
 * TODO 此处可以给小球增加动画效果
 * @param ballObj
 */
var firstBallList = [];
function checkBallFunction(ballObj) {
    firstBallList = [];//TODO 可以优化
    if(ballObj.y<=30){
        firstBallList.push(ballObj);
    }

    for (var i = 0; i < ballList.length; i++) {
        if (ballList[i].computFirst()&&ballList[i].y<60) {
            firstBallList.push(ballList[i]);
        }
    }
    if (firstBallList.length <= 0) {
        //TODO 通关
        return;
    }
    var tempBallList = [];
    if (checkBall.length >= 2) {
        for (var i = 0; i < ballList.length; i++) {
            if (ballList[i].canPop) {
                ballList[i].clearShape();
                ballList[i].remove();
            } else {
                tempBallList.push(ballList[i]);
            }
        }
        backLayer.removeChild(ballObj);
    } else {
        for (var i = 0; i < ballList.length; i++) {
            ballList[i].canPop = false;
        }
        ballList[ballList.length] = ballObj;//列表里加入新增的小球
    }
    if (tempBallList.length > 0) {
        ballList = tempBallList;
    }

    downUnLinkBall();
    checkBall = [];

}
/**
 * 掉下来未连接的小球,并处理
 * TODO 下落多少后进行销毁,展示每个分数
 */
function downUnLinkBall() {
    linkBallList = [];
    var downUnLinkBallList = [];

    for (var i = 0; i < firstBallList.length; i++) {
        selectSixLink(firstBallList[i]);
    }
    for (var i = 0; i < ballList.length; i++) {
        if (!containsLinkBall(ballList[i], linkBallList)) {
            downUnLinkBallList.push(ballList[i]);
            ballList[i].isDown = true;
        }
    }
};


/**
 * 归位小球到正确的位置
 * @param ballObj
 */
function ballInRightXY(ballObj) {
    for (var i = mapXyist.length-1; i >=0; i--) {
        var mapObj = mapXyist[i];
        var result = computBallXY(ballObj, mapObj);
        if (result < 35) {
            ballObj.x = mapObj.x;
            ballObj.y = mapObj.y
            ballObj.speed =0;
            ballObj.speedX=0;
            break;
        }
    }
}
/**
 * 根节点开始便利,如果有小球3个以上颜色相同,则消除
 * @param ballObj
 */
function checkRightXYBall() {
    linkBallList = [];
    if (firstBallList.length > 0) {
        for (var i = 0; i < firstBallList.length; i++) {
            selectSixLink(firstBallList[i]);
        }
    } else {
        //TODO 通关
    }
}
/**
 * 主函数
 * @param event
 */
function onframe(event) {

    if (oneBall) {

        for (var i = ballList.length-1; i >=0; --i) {
            var ballTemp = ballList[i];
            // 计算小球的圆心距离,如果小于2倍的半径(60)
            var result = computBallXY(ballObj, ballTemp);
            if (result <= 60 || ballObj.y<=30) {
                ballInRightXY(ballObj);//归位小球到正确的位置,时间开销无法优化
                selectSixShake(ballObj);//给周围6个球进行缓动
                for (var j = 0; j < shakeBallList.length; j++) {
                    selectSix(shakeBallList[j], ballObj.color);//递归全部颜色一样的球
                }
                shakeBallList = [];
                ballObj.speed = 0;
                ballObj.speedX = 0;
                //checkRightXYBall();
                setTimeout(function () {
                    checkBallFunction(ballObj);
                }, 500);

                oneBall = false;
                break;
            }
        }

    }
}




