/**
 * 递归算法查找6个颜色一样的小球,如果找到把ball.canPop=true
 */
function selectSix(ballObj,color) {
    if(color == ballObj.color){
        for (var i = 0; i < ballList.length; i++){
            var ballTemp = ballList[i];
            var result = computBallXY(ballObj, ballTemp);
            if (result <= 60) {
                //console.log("ballTemp"+ballTemp.color+"|ballObj"+ballObj.color);
                if(ballTemp.color == color){
                    if(!containsCheckBall(ballTemp)){
                        checkBall.push(ballTemp);
                        //console.log("checkBall"+checkBall);
                        ballList[i].canPop =true;
                        selectSix(ballTemp,color);
                    }
                }
            }
        }

    }
}

/**
 * 递归算法查找6个颜色一样的小球,如果找到把ball.canPop=true
 */
function selectSixLink(ballObj) {
    for (var i = 0; i < ballList.length; i++) {
        var ballTemp = ballList[i];
        //console.log("ballTemp"+ballTemp+"|ballObj"+ballObj);
        // 计算小球的圆心距离,如果小于2倍的半径(60)
        var result = computBallXY(ballObj, ballTemp);
        if (result <= 60) {
            //console.log("ballTemp"+ballTemp.color+"|ballObj"+ballObj.color);

            if(!containsLinkBall(ballTemp)){
                linkBallList.push(ballTemp);
                //console.log("linkBallList"+linkBallList);
                selectSixLink(ballTemp);
            }

        }
    }
}

/**
 * 查找6个晃动的小球,进行缓动操作
 */
function selectSixShake(ballObj) {
    for (var i = 0; i < ballList.length; i++) {
        var ballTemp = ballList[i];
        //console.log("ballTemp"+ballTemp+"|ballObj"+ballObj);
        // 计算小球的圆心距离,如果小于2倍的半径(60)
        var result = computBallXY(ballObj, ballTemp);
        if (result <= 60) {
            //console.log("ballTemp"+ballTemp.color+"|ballObj"+ballObj.color);

            if(!containsShakeBall(ballTemp)){
                shakeBallList.push(ballTemp);
            }
        }
    }

    for(var i=0;i<shakeBallList.length;i++){
        var shakeBall = shakeBallList[i];
        shakeBall.shake(ballObj.x,ballObj.y);
    }

}
