
function Ball(color,i,j,speedX,speed,x,y){
    base(this,LSprite,[]);
    var s = this;

    s.bitmap = new LBitmap(new LBitmapData(dataList["ball"+color]));
    s.addChild(s.bitmap);
    //console.log(i%2);
    s.pi=i%2;
    s.y = 30+i*50;
    s.pi==0?s.x = 20+j*60: s.x=j*60+50;
    if(x>0){
        s.x = x;
    }
    if(y>0){
        s.y = y;
    }
    //s.addBodyCircle(s.bitmap.getWidth()*0.5, s.bitmap.getHeight()*0.5, s.bitmap.getWidth()*0.5,1,0.5,.4,0.5);
    s.conPop = false;
    s.speed = speed
    s.speedX = speedX;
    s.vx =1;
    s.vy =1;
    s.color = color;
    s.isDown = false;
    s.addEventListener(LEvent.ENTER_FRAME, s.onframe);
};


/**
 * 递归两次，确保发射球碰到的球，以及这个被碰到的球的周围的球，发生相应弹动。弹动方向就是发射球与这个球的延长线上
 */
Ball.prototype.shake = function (x,y) {
    //var self = this;
    //var oldX = self.x;
    //var oldY = self.y;
    //var destX = x;
    //var destY = y;
    //console.log("oldX:"+oldX+"|oldY:"+oldY+"|destX:"+destX+"|destY:"+destY)
    ////TODO 缓动类,进行晃动
    //LTweenLite.to(self, 0.1, {x:destX, y:destY, onComplete:function comp(){
    //    LTweenLite.to(self, 0.1,{x:oldX,y:oldY,ease:LEasing.Sine.easeInOut});
    //}});
};
/**
 * 小球对象设置一个属性canPop，属性默认设置为false。
 * 这时我们要递归这个发射球，如果这个发射球周围6个位置有和他相同的颜色，那么把这个小球标记为canPop，
 * 同时递归这个球。（切记被递归过得不能再递归），
 * 最后遍历所有小球，如果canPop为true >=3,那么消除这几个球。
 */
Ball.prototype.markCanPopBall = function () {
    var self = this;
    self.conPop = true;
};
/**
 * 获取最顶端小球的数组，对这些球分别向下递归，
 * 递归到的就标记为处于连接状态，
 * 当所有球遍历一遍，那些处于未连接状态的就可以消失了
 */
Ball.prototype.checkDownBall = function () {
    var self = this;

};
/**
 * 给发射球添加辅助线，碰到边界要折回来
 * 这个线是若干点组成，这些点的X坐标要放到一个数组中，每个点的X坐标就是前一个点的X坐标+xxx（根据点与点间距获得每个点X坐标间距离）。
 * 当某个点的坐标碰到边界，那么这个点的X坐标位置为这个边界对面镜像位置，同时后面的坐标就是X坐标+（-xxx）.
 * 两边边界方法一样。
 */
Ball.prototype.imageBall = function () {
    var self = this;

};
Ball.prototype.computFirst = function () {
    var self = this;
    if(self.y<60){
        return true;
    }
    return false;
};

Ball.prototype.onframe=function (event){
    var self = event.currentTarget;
    if(self.speed!=0){
        self.y -= self.speed*self.vy;
        self.x += self.speedX*self.vx;
        if(self.y<30){
            self.y=30;
        }
    }
    if(self.isDown){
        self.y += 10;
        if(self.y>LGlobal.height-20){
            self.y=LGlobal.height-60;
            self.isDown = false;
        }
    }
    ////TODO 小球的边界
    if(self.x<20){
        self.x=20;
        self.vx = -1;
    }
    if(self.x>LGlobal.width-60){
        self.x=LGlobal.width-80;
        self.vx = -1;
    }


}