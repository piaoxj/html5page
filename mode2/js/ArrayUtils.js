/*****************数学begin******************/
/**
 * 获取不重复的数组
 * @param a
 * @returns {Array}
 */
function getArray(a) {
    var hash = {},
        len = a.length,
        result = [];

    for (var i = 0; i < len; i++){
        if (!hash[a[i]]){
            hash[a[i]] = true;
            result.push(a[i]);
        }
    }
    return result;
}

/**
 * 数组包含
 * @param element
 * @returns {boolean}
 */
function containsCheckBall(element){
    for (var i = 0; i < checkBall.length; i++) {
        if (checkBall[i] == element) {
            return true;
        }
    }
    return false;
}
/**
 * 数组包含
 * @param element
 * @returns {boolean}
 */
function containsShakeBall(element){
    for (var i = 0; i < shakeBallList.length; i++) {
        if (shakeBallList[i] == element) {
            return true;
        }
    }
    return false;
}
/**
 * 数组包含
 * @param element
 * @returns {boolean}
 */
function containsLinkBall(element){
    for (var i = 0; i < linkBallList.length; i++) {
        if (linkBallList[i] == element) {
            return true;
        }
    }
    return false;
}
/*****************数学end******************/
